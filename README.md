# TherioJunior

### Languages I speak:
![German](https://raw.githubusercontent.com/stevenrskelton/flag-icon/master/png/16/country-4x3/de.png) German
<br />
![English](https://raw.githubusercontent.com/stevenrskelton/flag-icon/master/png/16/country-4x3/gb.png) English

### IDEs & Text Editors I use:
<img align="left" alt="Visual Studio Code" width="26px" src="https://upload.wikimedia.org/wikipedia/commons/9/9a/Visual_Studio_Code_1.35_icon.svg" />
<img align="left" alt="Visual Studio 2019" width="26px" src="https://upload.wikimedia.org/wikipedia/commons/5/59/Visual_Studio_Icon_2019.svg" />
<img align="left" alt="PyCharm" width="26px" src="https://upload.wikimedia.org/wikipedia/commons/1/1d/PyCharm_Icon.svg" />
<img align="left" alt="PhpStorm" width="26px" src="https://www.b2x.cz/wp-content/uploads/2021/01/icon-phpstorm-768x768.png" />
<img align="left" alt="WebStorm" width="26px" src="https://cdn.hackr.io/uploads/posts/attachments/webstorm.png" />
<img align="left" alt="IntelliJ IDEA Ultimate" width="26px" src="https://cdn.hackr.io/uploads/posts/attachments/intellij-idea.png" />

<br />
<br />

### My Skills
- Virtual Machines (QEMU/KVM & VMWare)
- Windows Reverse Engineering
- Windows Kernel Drivers
- Docker
- Linux


### Programming Languages:
<img align="left" alt="C++" width="26px" src="https://upload.wikimedia.org/wikipedia/commons/1/18/ISO_C%2B%2B_Logo.svg" />
<img align="left" alt="C#" width="26px" src="https://cdn.icon-icons.com/icons2/2415/PNG/512/csharp_original_logo_icon_146578.png" />
<img align="left" alt="Java" width="26px" src="https://qph.fs.quoracdn.net/main-qimg-48b7a3d8958565e7aa3ad4dbf2312770" />
<img align="left" alt="Rust" width="26px" src="https://fyresite-wp-media.s3.us-west-2.amazonaws.com/wp-content/uploads/2020/09/29101345/Programming-Language-Rust.png" />

<br />
<br />

### Scripting Languages:
<img align="left" alt="LUA" width="26px" src="https://upload.wikimedia.org/wikipedia/commons/c/cf/Lua-Logo.svg" />
<img align="left" alt="Python" width="26px" src="https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg" />
<img align="left" alt="Shell" width="26px" src="https://cdn4.iconfinder.com/data/icons/document-file-types-green-set-03/338/File_type_Extension_148-512.png" />
<img align="left" alt="JavaScript" width="26px" src="https://upload.wikimedia.org/wikipedia/commons/9/99/Unofficial_JavaScript_logo_2.svg" />
<img align="left" alt="PHP" width="26px" src="https://i.pinimg.com/originals/40/72/d2/4072d2ad5f640ccbe88d9d5b82e2d702.png" />

<br />
<br />

### Markup Languages:
<img align="left" alt="HTML5" width="29px" src="https://upload.wikimedia.org/wikipedia/commons/6/61/HTML5_logo_and_wordmark.svg" />
<img align="left" alt="CSS" width="21px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CSS3_logo_and_wordmark.svg/120px-CSS3_logo_and_wordmark.svg.png" />

<br />
<br />

### Contact me at
<img align="left" alt="Discord" width="16px" src="https://camo.githubusercontent.com/f66a24788a2818b82624c61c17c513d16ea14ac6c579dd1c2b3ffe5df8c6cc22/68747470733a2f2f6564656e742e6769746875622e696f2f537570657254696e7949636f6e732f696d616765732f7376672f646973636f72642e737667"/> Discord - theriojunior

<img align="left" alt="Steam" width="16px" src="https://camo.githubusercontent.com/f1421340d45e319d31f5fa7e10853b8d70e388655a4ad3e15a66b6f8182a281c/68747470733a2f2f6564656e742e6769746875622e696f2f537570657254696e7949636f6e732f696d616765732f7376672f737465616d2e737667"/> Steam - [id/hypnotizable](https://steamcommunity.com/id/hypnotizable/)

<img align="left" alt="YouTube" width="16px" src="https://camo.githubusercontent.com/a57a6d6ab9659de0e830d117339e092c7976177a309572210383374fbfd2e004/68747470733a2f2f6564656e742e6769746875622e696f2f537570657254696e7949636f6e732f696d616765732f7376672f796f75747562652e737667"/>YouTube - [/@TherioJunior](https://youtube.com/@TherioJunior)

<img align="left" alt="Odysee" width="16px" src="https://killstream.live/wp-content/uploads/2021/03/odysee-1.jpg"/> Odysee - [/@TherioJunior](https://odysee.com/@TherioJunior:e)
